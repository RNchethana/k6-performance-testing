import http from 'k6/http'
import {check} from 'k6'

export default function()
{
//Main function where you will get spread in Google API
http.get('https://test-api.k6.io/public/crocodiles/');
//Auto forma

//to run mutiple vusers k6 run ./Test/01.Script.js --vus 10 --duration 10s
check (response ,{
    'is response status is 200 :' :(r) => r.status ===200,
    'body size is byte:' :(r) =>r.body.length ==30,
})

let body = JSON.parse(response.body)
console.log('response body is ${body}')
console.log('Message is ${body.message}')
}