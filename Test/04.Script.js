//https://run.mocky.io/v3/c4016d63-bd75-49cd-b4c9-dd65f9b0600f

import http from 'k6/http'
import {check, sleep} from 'k6'
import { Trend } from 'k6/metrics' 
//define trend variable
var getAPItrend = new Trend('get_TREND_API_DURATION')

export const options = {
   stages: [
     { duration: '10s', target: 15 },
     { duration: '10s', target: 15 },
     { duration: '30s', target: 0 },
   ],
 };


export default function(){
    let response = http.get('https://run.mocky.io/v3/c4016d63-bd75-49cd-b4c9-dd65f9b0600f')

    check (response ,{
        'is response status is 200 :' :(r) => r.status ===200,
               })
//added response in custom trend
   getAPItrend.add(response.timings.duration)

  sleep(1) 
}
