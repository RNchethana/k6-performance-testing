//https://run.mocky.io/v3/c4016d63-bd75-49cd-b4c9-dd65f9b0600f

import http from 'k6/http'
import {check, sleep} from 'k6'
import { Trend } from 'k6/metrics' 
//define trend variable
var getAPItrend = new Trend('get_TREND_API_DURATION')


export let options= {
thresholds:{
   'checks':['rate>0.95']
}
}

export default function(){
    let response = http.get('https://test.k6.io')


    check (response ,{
        'is response status is 200 :' :(r) => r.status ===200,
        //'body size is byte:' :(r) =>r.body.length ==30,
   })
//added response in custom trend
   getAPItrend.add(response.timings.duration)
//https://test.k6.io
  sleep(1) 
}