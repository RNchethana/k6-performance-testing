import http from 'k6/http'
import {check} from 'k6'

export default function()
{
let response =http.get('https://run.mocky.io/v3/8bb4ece5-9482-4ac1-8706-2cd2ff867ce0')
//print logs

console.log(`response body length ${response.body.length} for VU = ${__VU} ITERA = ${__ITER}`)

check(response ,{
     'is response status is 200 :' :(r) => r.status ===200,
     'body size is  byte:' :(r) =>r.body.length ==0,
})
}
